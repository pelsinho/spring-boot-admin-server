FROM adoptopenjdk/maven-openjdk11

EXPOSE 8090 587

ENV APP_USER="app"
ENV MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
ENV MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

MAINTAINER nelsongomes.com

#In Progress
#COPY checkstyle.xml /tmp/
#COPY pom.xml /tmp/
#COPY src /tmp/src/
#WORKDIR /tmp/
#RUN mvn clean package -Dmaven.test.skip=true -DskipTests -Djacoco.skip=true

COPY target/admin.server.jar admin.server.jar

ENTRYPOINT ["java","-jar","/admin.server.jar"]
