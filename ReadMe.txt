
## Title

  Spring Boot Monitor Admin Server

## Goal

  Monitor Spring Boot Applications

## Tooling

  1. Java 11
  2. Maven
  3. Application is configure to use 8090 http port. If you need please switch on application.properties (server.port), or adding a Env_Variable like : SERVER_HTTP_PORT = 8090

  If you change item 4, please don´t forget to adjust Spring Admin Clients.

## Installation

  1. Add this project in any computer directory

## Compilation

  1. Go to project root folder
  2. Compile the project using : mvn clean install. You need to install maven in your computer.

## Start

  1. Go to project root folder
  2. Start the application using : mvn spring-boot:run. You need to install maven in your computer.
  3. Access Link : localhost:8090
  4. User : admin
  5. Password : admin


########################
# Server Configuration #
############# ###########

Spring Boot Admin Home Page

    http://localhost:8090/login

Add to Application.class

    @EnableAdminServer

Please Add to POM Spring Admin Server

    <dependency>
        <groupId>de.codecentric</groupId>
        <artifactId>spring-boot-admin-server-ui-login</artifactId>
        <version>1.5.7</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
        <version>2.4.0</version>
    </dependency>

If needed Spring Boot Admin can be configured to display only the information that we consider useful.
We just have to alter the default configuration and add our own needed metrics:

    spring.boot.admin.routes.endpoints=env, metrics, trace, jolokia, info, configprops


########################
# Client Configuration #
########################

POM :

    <dependency>
        <groupId>de.codecentric</groupId>
        <artifactId>spring-boot-admin-server-ui-login</artifactId>
        <version>1.5.7</version>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>


Spring Boot Admin Properties :

    - management.endpoints.web.base-path=/manage
    - management.endpoints.web.exposure.include=*
    - management.endpoint.health.show-details=always
    - spring.boot.admin.client.url=http://localhost:8090
    - spring.boot.admin.client.username=admin
    - spring.boot.admin.client.password=admin
